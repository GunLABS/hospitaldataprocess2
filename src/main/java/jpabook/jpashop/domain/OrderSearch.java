package jpabook.jpashop.domain;

public class OrderSearch {

    private String MemberName; // 회원이름
    private OrderStatus orderStatus; // 주문상태

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}
